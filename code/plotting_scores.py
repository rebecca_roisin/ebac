import csv
from matplotlib import pyplot as plt
import numpy

# read data into lists
ma3_actual = []
ma5_actual = []
ma3_pred = []
ma5_pred = []
pred3_all = []
pred5_all = []

f = open("../scratch/math_scores.csv")
f_csv = csv.reader(f, delimiter=',')
next(f_csv, None) 
for row in f_csv:
	if not ((row[1] == "nan") & (row[2] == "nan")):
		pred5_all.append(float(row[4]))
		pred3_all.append(float(row[3]))
		 
		if row[1] == "nan": # student took ma5
			ma5_actual.append(float(row[2]))
			ma5_pred.append(float(row[4]))
		elif row[2] == "nan":	# student took ma3
			ma3_actual.append(float(row[1]))
			ma3_pred.append(float(row[3]))
f.close()

print len(ma3_actual), len(ma3_pred)
print len(pred3_all), len(pred5_all)

# print out means
print "Ma3: Actual: %s, Pred: %s" %(numpy.mean(ma3_actual), numpy.mean(ma3_pred))
print "Ma5: Actual: %s, Pred: %s" %(numpy.mean(ma5_actual), numpy.mean(ma5_pred))

# plot ma3 scores
plt.xlim(0.0, 10.0)
plt.ylim(0.0, 10.0)
plt.grid(True)

plt.scatter(ma3_actual, ma3_pred, s=10, alpha=0.1)
plt.savefig("../scratch/ma3_scatter.pdf")

plt.cla()

# plot ma5 scores
plt.xlim(0.0, 10.0)
plt.ylim(0.0, 10.0)
plt.grid(True)

plt.scatter(ma5_actual, ma5_pred, s=10, alpha=0.1)
plt.savefig("../scratch/ma5_scatter.pdf")

plt.cla()

# make histograms of scores
# ma3
bins = numpy.arange(0, 10, 0.25)
n3a, b3a, p3a = plt.hist(ma3_actual, bins=bins, histtype="step", color="r", label = "actual", normed=1)
n3p, b3p, p3p = plt.hist(ma3_pred, bins=bins, histtype="step", color="b", label = "predicted", normed=1)
na3p, ba3p, pa3p = plt.hist(pred3_all, bins=bins, histtype="step", color="g", label = "all pred", normed=1)
legend = plt.legend()
plt.savefig("../scratch/ma3_histogram.pdf")
plt.cla()

# ma5
print pred5_all
n5a, b5a, p5a = plt.hist(ma5_actual, bins=bins, histtype="step", color="r", label = "actual", normed=1)
n5p, b5p, p5p = plt.hist(ma5_pred, bins=bins, histtype="step", color="b", label = "predicted", normed=1)
na5p, ba5p, pa5p = plt.hist(pred5_all, bins=bins, histtype="step", color="g", label = "all pred", normed=1)
legend = plt.legend()
plt.savefig("../scratch/ma5_histogram.pdf")
