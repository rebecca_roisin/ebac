import numpy
import pickle
import random
import math as ma
from collections import Counter
from multiprocessing import Pool
from copy import copy

from BACanalysisForest import student, all_subjects
from pandas import *

import cProfile


def split_fast(records, feature):
  fname, fvalue = feature

  no_score = records[numpy.isnan(records[fname])]
  left = records[records[fname] < fvalue ]
  right = records[fvalue <= records[fname]]
  return no_score, left, right

def fast_entropy(rec_list, labels):
  if len(rec_list) == 0:
    return 0

  rec_list = rec_list[labels].as_matrix()
  mdat = numpy.ma.masked_array(rec_list, numpy.isnan(rec_list))
  variance = numpy.var(mdat,axis=0)

  # determine if values or not:
  present = numpy.isnan(rec_list)
  probs = numpy.mean(present, axis=0)
  
  var = variance[probs < 1.0]
  ent = numpy.sum((2 * numpy.pi * ma.e * var) ** 0.5)

  bi = probs[(probs != 1.0) & (probs != 0.0)]
  nbi = 1.0 - bi
  binent = numpy.sum(-(bi * numpy.log2(bi) + nbi * numpy.log2(nbi)))

  return binent + ent

def score(records, feature, labels, split=split_fast, entropy = fast_entropy):
  tot_rec = len(records)
  fname, fvalue = feature

  no_score, smaller, higher = split(records, feature)
  ent_none = entropy(no_score, labels)
  ent_left = entropy(smaller, labels)
  ent_right = entropy(higher, labels)
  ent_total = entropy(records, labels)
  p_0 = float(len(no_score)) / tot_rec
  p_L = float(len(smaller)) / tot_rec
  p_R = float(len(higher)) / tot_rec
  #print "Lengths: %s, %s, %s, %s, %s, %s" %(p_0, p_L, p_R, ent_none, ent_left, ent_right)
  score = ent_total - ((p_0 * ent_none) + (p_L * ent_left) + (p_R * ent_right))
  return score


class RandomTree:
  def __init__(self, feature_names, label_names):
    self.feature_names = feature_names
    self.label_names = label_names
    self.tree = None
    self.debug = False

  def train(self, records, max_depth = 6, feature_number = 40, min_bucket = 40):
    self.tree = self._train(records, 0, max_depth, feature_number, min_bucket)

  def _train(self, records, depth, max_depth, feature_number, min_bucket):

    ## Do not split beyond a point
    if depth >= max_depth or len(records) < min_bucket:
      return (1, records) # 1 - means it is a leaf 

    ## Pick random features from the dataset
    features = []
    while len(features) < feature_number:
      r = random.choice(records.index.values)
      i = random.choice(self.feature_names)
      v = records[i][r]

      if numpy.isnan(v):
        continue
      features += [(i, v)]

    scores = [(score(records, f, self.label_names), f) for f in features]
    _ , f = max(scores)

    N,L,R = split_fast(records, f)

    if self.debug:
      try:
        print "Split on %s : %2.2f" % (f[0], f[1])
        print "Records len:", len(records)
        print "None: %d Less than: %d Greater than: %d" % (len(N), len(L), len(R))
        for sc, (idx, cut) in scores:
          print "%s < %2.2f : %2.2f" % (idx, cut, sc)
        print
      except:
        print idx, names 
        raise

    Tnone =  self._train(N, depth+1, max_depth, feature_number, min_bucket)
    Tright = self._train(R, depth+1, max_depth, feature_number, min_bucket)
    Tleft =  self._train(L, depth+1, max_depth, feature_number, min_bucket)
       
    return (0, f, Tnone, Tright, Tleft) # 0 - means it is a branch


  def classify(self, record):
    if self.tree == None:
      raise Exception("You must train the tree first!")
    
    node = self.tree
    while node[0] == 0:
      _, (fname, fvalue), N, R, L = node
      v = record[fname]
      if numpy.isnan(v):
        node = N 
        continue
      if v < fvalue:
        node = L
      else:
        node = R

    _, saved_records = node
    data = saved_records[self.label_names].as_matrix()
    return numpy.mean(data, axis = 0)


def build_and_train_tree(p):
  feature_names, label_names, records, max_depth, feature_number, min_bucket = p
  T = RandomTree(feature_names, label_names)
  T.train(records, max_depth, feature_number, min_bucket)
  return T  


class RandomForest:
  def __init__(self, feature_names, label_names, tree_number):
    self.feature_names = feature_names
    self.label_names = label_names
    self.tree_number = tree_number
    self.trees = None
    self.debug = False

  def train(self, records, max_depth = 8, feature_number = 40, min_bucket = 30):
    
    params = []
    for _ in range(self.tree_number):
      params += [(self.feature_names, self.label_names, records, max_depth, feature_number, min_bucket)]

      # T = RandomTree(self.feature_names, self.label_names)
      # T.train(records, max_depth, feature_number, min_bucket)
    
    pool = Pool(processes=8)    
    self.trees = pool.map(build_and_train_tree, params)

  # pool = Pool(processes=8)              # start 4 worker processes
  # all_trees = pool.map(tree_maker, [records] * 10)          # prints "[0, 1, 4,..., 81]"


  def classify(self, record):
    result = numpy.array([T.classify(record) for T in self.trees])
    return numpy.mean(result, axis=0), numpy.std(result, axis=0)

if __name__ == "__main__":

  # Load all the data
  f = file("../data/BAC.csv")

  titles = f.readline()

  # Load all student records
  all_students = {}
  target_subject = "ma5"

  for student_record in f:
    this_student = student(student_record)
    # Check if we already have a recod for this student
    if this_student.id in all_students:
      this_student = all_students[this_student.id]
    else:
      all_students[this_student.id] = this_student
    this_student.set_score(student_record)

  f.close()

  # Make a list of all students
  student_dict = all_students
  

  all_students = all_students.values()

  n, sub = all_subjects(all_students)
  print "Number of students:", n
  print "Subjects"
  print sub

  popular_subjects = sorted([s for s,p in sub.iteritems() if p > 50], 
    key=lambda x:-sub[x])
  
  print popular_subjects

  ## Test records for objective function
  records = numpy.array([s.get_vectors(popular_subjects) for s in all_students])
  # print records[:10]
  student_frame = DataFrame(records[:,1:], index=records[:,0], columns=popular_subjects)
  r = random.choice(student_frame.index.values)
  print "Random", r
  print "Element", student_frame["l1-"][r]
  
  feature_names = copy(popular_subjects)
  feature_names.remove(target_subject)
  mytree = RandomForest(feature_names, [target_subject], 100)
  # mytree.debug = True
  training_data = student_frame[~numpy.isnan(student_frame[target_subject])]
  mytree.train(training_data)

  stu_id = []
  stu_score = []
  mean_score = []
  std_score = []
  for index, row in student_frame.iterrows():
    print row[target_subject], mytree.classify(row)
    mean_score.append(mytree.classify(row)[0][0])
    std_score.append(mytree.classify(row)[1][0])
    stu_id.append(index)
    stu_score.append(row[target_subject])
    #output_data.write("%s,%s,%s,%s\n" %(index, row["ma3"], mytree.classify(row)[0], mytree.classify(row)[1]))
  
  # make pandas data frame
  target_subject_scores = DataFrame(stu_score, index = stu_id, columns = ["Actual Score"])
  target_subject_scores["Mean Prediction"] = mean_score
  target_subject_scores["Std Prediction"] = std_score
  target_subject_scores.to_csv("../scratch/%s_scores.csv" %(target_subject), sep=",")



  # # making trees
  # synthetic = {}

  # pool = Pool(processes=8)              # start 4 worker processes
  # all_trees = pool.map(tree_maker, [records] * 10)          # prints "[0, 1, 4,..., 81]"

  # for i, tree_result in enumerate(all_trees):
  #   print "Making Tree %s" % i
  #   #cProfile.run("make_tree(records, names = popular_subjects)")
  #   names = ["ID"] + popular_subjects
  #   for x in tree_result:

  #     if len(x) == 0:
  #       continue

  #     scores = numpy.array([student_dict[int(rec[0])].get_vectors(["ma3","ma5"]) for rec in x])

  #     scores = scores[:, 1:]
  #     count_scores = numpy.sum(~numpy.isnan(scores), axis=0)
  #     masked_scores = numpy.ma.masked_array(scores, numpy.isnan(scores))
  #     sum_scores = numpy.sum(masked_scores, axis=0).filled(0.0)

  #     # print scores
  #     # print "Average scores", numpy.mean(masked_scores, axis=0)
  #     # print "Number scores", average_scores, count_scores

  #     for rec in x:
  #       rec_id = int(rec[0])
  #       if rec_id not in synthetic:
  #         synthetic[rec_id] = numpy.array([[0,0],[0,0]])

  #       actual = student_dict[rec_id].get_vectors(["ma3", "ma5"])[1:]
  #       actual[numpy.isnan(actual)] = 0.0
  #       synthetic[rec_id][0] += (sum_scores - actual)
  #       actual[actual > 0.0] = 1.0
  #       synthetic[rec_id][1] += count_scores - actual
    
  # output_file = open("../scratch/math_scores.csv", "w")
  # #ma3_score_file = open("math3_scores.csv", "w")
  # #ma5_score_file = open("math5_scores.csv", "w")
  # output_file.write("%s,%s,%s,%s,%s\n" %("Record", "Ma3_actual", "Ma5_actual", "Ma3_pred", "Ma5_pred"))
  # for rec_id in synthetic.iterkeys():
  #   ma3_score = student_dict[int(rec_id)].get_vectors(["ma3"])[1]
  #   ma5_score = student_dict[int(rec_id)].get_vectors(["ma5"])[1]
  #   ma3_n, ma5_n = synthetic[rec_id][0]
  #   ma3_d, ma5_d = synthetic[rec_id][1]
  #   if ma3_d == 0:
  #     ma3_pred = float("NaN")
  #   else:
  #     ma3_pred = float(ma3_n) / float(ma3_d)
  #   if ma5_d == 0:
  #     ma5_pred = float("NaN")
  #   else:
  #     ma5_pred = float(ma5_n) / float(ma5_d)
  #   output_file.write("%s,%s,%s,%s,%s\n" %(rec_id, ma3_score, ma5_score, ma3_pred, ma5_pred)) 
  #   #if numpy.isnan(ma3_score) == True: # student took ma5


  # output_file.close() 


  # #for _ in range(100):
  # # make_tree(records, names = None)


  # #pool = Pool(processes=8)              # start 4 worker processes
  # #res = pool.map(make_tree, [records] * 10)          # prints "[0, 1, 4,..., 81]"
  # #print "Results", len(res)