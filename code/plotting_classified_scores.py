from matplotlib import pyplot as plt
import numpy
from pandas import *

# read into pandas dataframe
scores = DataFrame.from_csv("../scratch/ma3_scores.csv",  header=0, index_col = 0, sep=",")

# separate into those who took ma3 and those who did not
selector = numpy.isnan(scores["Actual Score"])
training = scores[~selector]
test = scores[selector]

# plot scores
plt.scatter(training["Actual Score"], training["Mean Prediction"], s=10, alpha=0.1)
plt.savefig("../scratch/ma3_scores_scatter_classify.pdf")
plt.cla()

# plot score histogram
bins = np.arange(0, 10, 0.25)
n3a, b3a, p3a = plt.hist(training["Actual Score"], bins=bins, histtype="step", color="r", label = "actual", normed=1)
n3train, b3train, p3train = plt.hist(training["Mean Prediction"], bins=bins, histtype="step", color="b", label = "training", normed=1)
n3test, b3test, p3test = plt.hist(test["Mean Prediction"], bins=bins, histtype="step", color="g", label = "test", normed=1)
legend = plt.legend(loc=2)
plt.savefig("../scratch/ma3_classify_scores_histogram.pdf")
#plt.show()
plt.cla()

bins = np.arange(0, 10, 0.25)
n3a, b3a, p3a = plt.hist([training["Actual Score"], test["Mean Prediction"]], bins=bins, histtype="step", color=["r", "g"], label = ["actual", "prediction"], normed=0, stacked=0)
#n3test, b3test, p3test = plt.hist(test["Mean Prediction"], bins=bins, histtype="step", color="g", label = "test", normed=1, stacked=1)
legend = plt.legend(loc=2)
plt.savefig("../scratch/ma3_unstacked_scores.pdf")
#plt.show()
plt.cla()


# plot stacked histogram of actual + predicted scores
bins = np.arange(0, 10, 0.25)
n3a, b3a, p3a = plt.hist([training["Actual Score"], test["Mean Prediction"]], bins=bins, histtype="step", color=["r", "g"], label = ["actual", "prediction"], normed=0, stacked=1)
#n3test, b3test, p3test = plt.hist(test["Mean Prediction"], bins=bins, histtype="step", color="g", label = "test", normed=1, stacked=1)
legend = plt.legend(loc=2)
plt.savefig("../scratch/ma3_stacked_scores.pdf")
#plt.show()
plt.cla()

# get percentiles
# ma3 percentiles
test_scores = test["Mean Prediction"][~numpy.isnan(test["Mean Prediction"])].copy()
actual_scores = training["Actual Score"].copy()

ma3_scores = concat([actual_scores, test_scores])
ma3_scores.columns = ["score"]
ma3_scores.sort()
ma3_scores = DataFrame(ma3_scores, columns=["score"])

ma3_scores["pc"] = [round(float(seq) / len(ma3_scores),2) for seq, _ in enumerate(ma3_scores.iterrows())]

actual_scores.sort()
actual_scores = DataFrame(actual_scores, columns=["score"])
actual_scores["actual_pc"] = [round(float(seq) / len(actual_scores),2) for seq, _ in enumerate(actual_scores.iterrows())]
actual_scores["estimated_pc"] = [ma3_scores.at[index,"pc"] for index, _ in actual_scores.iterrows()]


g = numpy.arange(0, 1, 0.001)
plt.plot(actual_scores["actual_pc"], actual_scores["estimated_pc"], 'b')
plt.plot(g, g, 'k--')
plt.grid(True)
plt.xlim(0, 1, 0.1)
plt.ylim(0, 1, 0.1)
plt.savefig("../scratch/corrected_pc.pdf")
#plt.show()
plt.cla()

def get_score(pc, frame):
	return np.mean(actual_scores["score"][actual_scores["actual_pc"] == pc])

actual_scores["adjusted_score"] = [get_score(i,actual_scores) for i in actual_scores["estimated_pc"]]

print actual_scores.to_string()

h = np.arange(0, 10, 0.01)
plt.plot(h, h, 'k--')
plt.scatter(actual_scores["score"], actual_scores["adjusted_score"], s=10, marker="x")
plt.xlim(0, 10, 1)
plt.ylim(0, 10, 1)
plt.grid(True)
plt.savefig("../scratch/corrected_grade.pdf")
plt.show()
plt.cla()
# print ma3_scores.to_string()


# get corrected score for ma3
#for index, row in ma3_scores.iterrows():
#	act_score = row[score]
#	true_pc = row["pc"]
#	correc_pc = 