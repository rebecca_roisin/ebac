import numpy
import pickle
import random
import math as ma
from collections import Counter
from multiprocessing import Pool

import cProfile

class student:
  
  # Initialize the student record
  def __init__(self, string_s):
    # split the string into records
    s = string_s.strip()
    parts = s.split(",")
    self.id = int(parts[3])
    self.parts = parts[:9]
    self.scores = {}
    self.average = None
    self.corrected_scores = {}
    self.corrected_average = None

  # Set a score in a particular subject for this student
  def set_score(self, string_s):
    s = string_s.strip()
    parts = s.split(",")
    self.scores[parts[9]] =  float(parts[11])    

  def __repr__(self):
    return self.__str__()

  def __str__(self):
    Str = "Student %s" % self.id
    for subject in self.scores:
      add_s = ""
      if subject in self.corrected_scores:
        add_s = " (%2.2f)" % self.corrected_scores[subject]
      Str += " %s: %s %s" % (subject, self.scores[subject], add_s)
    return Str

  def get_vectors(self, subjects):
    # subj = numpy.array([1 if s in self.scores else 0 for s in subjects])
    grad = numpy.array([self.id] + [self.scores[s] if s in self.scores else numpy.nan for s in subjects])
    return grad

def all_subjects(records):
  num = len(records)
  c = Counter()
  for r in records:
    c.update(r.scores.keys())
  return num, c

def split_fast(records, feature, value):
  no_score = records[numpy.isnan(records[:, feature])]
  left = records[records[:, feature] < value]
  right = records[records[:, feature] >= value]
  return numpy.array(no_score), numpy.array(left), numpy.array(right)

def split(records, feature, value):
  no_score = []
  left = []
  right = []
  for rec in records:
    if numpy.isnan(rec[feature]):
      no_score.append(rec)
    elif rec[feature] >= value:
      right.append(rec)
    elif rec[feature] < value:
      left.append(rec)
    else:
      print "The record is weird somehow."
      print rec, feature, value
      raise Exception("The record is weird somehow.")
  return no_score, left, right

def fast_entropy(rec_list):
  if len(rec_list) == 0:
    return 0

  rec_list = rec_list[:, 1:]
  
  num_scores = len(rec_list[0])
  mdat = numpy.ma.masked_array(rec_list, numpy.isnan(rec_list))
  variance = numpy.var(mdat,axis=0)

  # determine if values or not:
  present = numpy.isnan(rec_list)
  probs = numpy.mean(present, axis=0)
  #print probs
  var = variance[probs < 1.0]
  ent = numpy.sum((2 * numpy.pi * ma.e * var) ** 0.5)

  bi = probs[(probs != 1.0) & (probs != 0.0)]
  nbi = 1.0 - bi

  binent = numpy.sum(-(bi * numpy.log2(bi) + nbi * numpy.log2(nbi)))

  return binent + ent


def entropy(rec_list):
  if len(rec_list) == 0:
    return 0

  rec_list = rec_list[:, 1:]

  num_scores = len(rec_list[0])   # number of subjects under consideration
  num_records = len(rec_list)
  ents = []
  for i in range(num_scores):
    not_taken = 0
    taken = []
    for j in rec_list:
      if numpy.isnan(j[i]):
        not_taken += 1
      else:
        taken.append(j[i])
    #print taken

    # Binary variance
    prob_taken = float(len(taken)) / len(rec_list)
    prob_not_taken = 1.0 - prob_taken

    if prob_taken > 0 and prob_not_taken > 0:
      binent = - (prob_taken * ma.log(prob_taken, 2.0) + prob_not_taken * ma.log(prob_not_taken, 2.0))
    else:
      binent = 0.0

    if len(taken) == 0:
      ents.append(binent)
    else:
      variance = numpy.var(taken)
      ent = (2 * numpy.pi * ma.e * variance) ** 0.5
      ents.append(binent + ent)
  return sum(ents)

def score(records, feature, value, split=split_fast, entropy = fast_entropy):
  tot_rec = len(records)
  no_score, smaller, higher = split(records, feature, value)
  ent_none = entropy(no_score)
  ent_left = entropy(smaller)
  ent_right = entropy(higher)
  ent_total = entropy(records)
  p_0 = float(len(no_score)) / tot_rec
  p_L = float(len(smaller)) / tot_rec
  p_R = float(len(higher)) / tot_rec
  #print "Lengths: %s, %s, %s, %s, %s, %s" %(p_0, p_L, p_R, ent_none, ent_left, ent_right)
  score = ent_total - ((p_0 * ent_none) + (p_L * ent_left) + (p_R * ent_right))
  return score



def make_tree(records, depth=0, max_depth = 8, feature_number = 40, min_bucket = 10, names = None):

  ## Do not split beyond a point
  if depth >= max_depth or len(records) < min_bucket:
    #return (0, records)
    yield records
    return 

  if names != None:
      assert len(records[0]) == len(names)


  ## Pick random features from the dataset
  features = []
  assert len(records) > 0
  while len(features) < feature_number:
    r = random.choice(records)
    i = random.randint(1, len(r)-1)
    if numpy.isnan(r[i]):
      continue
    features += [(i, r[i])]

  scores = sorted([(score(records, x[0],x[1]), x) for x in features], reverse=True)
  _ , f = max(scores)

  N,L,R = split_fast(records, f[0], f[1])
  if names != None:
    try:
      print "Split on %s : %2.2f" % (names[f[0]], f[1])
      print "Records len:", len(records)
      print "None: %d Less than: %d Greater than: %d" % (len(N), len(L), len(R))
      for sc, (idx, cut) in scores:
        print "%s < %2.2f : %2.2f" % (names[idx], cut, sc)
      print
    except:
      print idx, names 
      raise
    

  #return (1, f, 
  #  make_tree(N, depth+1, max_depth, feature_number, names), 
  #  make_tree(R, depth+1, max_depth, feature_number, names), 
  #  make_tree(L, depth+1, max_depth, feature_number, names))
  for x in make_tree(N, depth+1, max_depth, feature_number, min_bucket, names):
    yield x
  for x in make_tree(R, depth+1, max_depth, feature_number, min_bucket, names):
    yield x
  for x in make_tree(L, depth+1, max_depth, feature_number, min_bucket, names):
    yield x 
  return

def tree_maker(records):
    return list(make_tree(records))
 

if __name__ == "__main__":

  # Load all the data
  f = file("../data/BAC.csv")

  titles = f.readline()

  # Load all student records
  all_students = {}

  for student_record in f:
    this_student = student(student_record)
    # Check if we already have a recod for this student
    if this_student.id in all_students:
      this_student = all_students[this_student.id]
    else:
      all_students[this_student.id] = this_student
    this_student.set_score(student_record)

  f.close()

  # Make a list of all students
  student_dict = all_students
  all_students = all_students.values()

  n, sub = all_subjects(all_students)
  print "Number of students:", n
  print "Subjects"
  print sub

  popular_subjects = sorted([s for s,p in sub.iteritems() if p > 50], 
    key=lambda x:-sub[x])
  popular_subjects.remove("ma5")
  popular_subjects.remove("ma3")
  
  print popular_subjects

  ## Test records for objective function
  records = numpy.array([s.get_vectors(popular_subjects) for s in all_students])
  # print records[:10]


  recs = numpy.array([numpy.array([float('NaN'), 2, 3]), numpy.array([1, 4, float('NaN')]), numpy.array([2, 5, 15])])
  #fast_entropy(recs)
  for i in range(len(recs)):
    print recs[i], recs[i][1]
  for i in range(3):
    for j in range(0, 10, 1):
      m1 = score(recs, i, j, split_fast, entropy)
      m2 = score(recs, i, j, split_fast, fast_entropy)
      print "(%s, %s): %s, %s" %(i,j, m1, m2)


  # making trees
  synthetic = {}

  pool = Pool(processes=8)              # start 4 worker processes
  all_trees = pool.map(tree_maker, [records] * 100)          # prints "[0, 1, 4,..., 81]"

  for i, tree_result in enumerate(all_trees):
    print "Making Tree %s" % i
    #cProfile.run("make_tree(records, names = popular_subjects)")
    names = ["ID"] + popular_subjects
    for x in tree_result:

      if len(x) == 0:
        continue

      scores = numpy.array([student_dict[int(rec[0])].get_vectors(["ma3","ma5"]) for rec in x])

      scores = scores[:, 1:]
      count_scores = numpy.sum(~numpy.isnan(scores), axis=0)
      masked_scores = numpy.ma.masked_array(scores, numpy.isnan(scores))
      sum_scores = numpy.sum(masked_scores, axis=0).filled(0.0)

      # print scores
      # print "Average scores", numpy.mean(masked_scores, axis=0)
      # print "Number scores", average_scores, count_scores

      for rec in x:
        rec_id = int(rec[0])
        if rec_id not in synthetic:
          synthetic[rec_id] = numpy.array([[0,0],[0,0]])

        actual = student_dict[rec_id].get_vectors(["ma3", "ma5"])[1:]
        actual[numpy.isnan(actual)] = 0.0
        synthetic[rec_id][0] += (sum_scores - actual)
        actual[actual > 0.0] = 1.0
        synthetic[rec_id][1] += count_scores - actual
    
  output_file = open("scratch/math_scores.csv", "w")
  #ma3_score_file = open("math3_scores.csv", "w")
  #ma5_score_file = open("math5_scores.csv", "w")
  output_file.write("%s,%s,%s,%s,%s\n" %("Record", "Ma3_actual", "Ma5_actual", "Ma3_pred", "Ma5_pred"))
  for rec_id in synthetic.iterkeys():
    ma3_score = student_dict[int(rec_id)].get_vectors(["ma3"])[1]
    ma5_score = student_dict[int(rec_id)].get_vectors(["ma5"])[1]
    ma3_n, ma5_n = synthetic[rec_id][0]
    ma3_d, ma5_d = synthetic[rec_id][1]
    if ma3_d == 0:
      ma3_pred = float("NaN")
    else:
      ma3_pred = float(ma3_n) / float(ma3_d)
    if ma5_d == 0:
      ma5_pred = float("NaN")
    else:
      ma5_pred = float(ma5_n) / float(ma5_d)
    output_file.write("%s,%s,%s,%s,%s\n" %(rec_id, ma3_score, ma5_score, ma3_pred, ma5_pred)) 
    #if numpy.isnan(ma3_score) == True: # student took ma5


  output_file.close() 


  #for _ in range(100):
  # make_tree(records, names = None)


  #pool = Pool(processes=8)              # start 4 worker processes
  #res = pool.map(make_tree, [records] * 10)          # prints "[0, 1, 4,..., 81]"
  #print "Results", len(res)